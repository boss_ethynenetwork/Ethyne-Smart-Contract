pragma solidity ^0.4.24;
import "openzeppelin-solidity/contracts/math/SafeMath.sol";

contract EthyneEscrow{
  using SafeMath for uint;
  using SafeMath for uint8;
  using SafeMath for uint256;
  using SafeMath for uint128;
  using SafeMath for uint16;

  address public owner;
  address public relayer;
  uint256 public ethyneCollectedFees;
  uint32 public buyerPaymentWindow;

  struct Escrow {
    bool _isActive;
    uint32 _enableSellerCancellation;
    uint256 _gasUsedByRelayer;
  }

  mapping (bytes32 => Escrow) public escrows;

  modifier onlyOwner(){
    require(msg.sender == owner);
    _;
  }

  modifier onlyRelayer(){
    require(msg.sender == relayer);
    _;
  }

  //events
  event LogCreated(
    bytes32 _trade_id,
    address _buyer,
    address _seller
  );

  event LogFundReleased(
    bytes32 _trade_id,
    address _buyer,
    address _seller
  );

  event LogBuyerMarkedAsPaidOrOpenDispute(
    bytes32 _trade_id
  );

  event LogSellerCancelled(
    bytes32 _trade_id,
    address _buyer,
    address _seller
  );

  event LogBuyerCancelled(
    bytes32 _trade_id,
    address _buyer,
    address _seller
  );

  event LogResolveDispute(
    bytes32 _trade_id,
    address _buyer,
    address _seller
  );

  // constructor for escrow
  constructor() public{
    owner = msg.sender;
    relayer = msg.sender;
    buyerPaymentWindow = 3600;
  }

  function createEscrow(
    bytes32 _tradeID,
    address _seller,
    address _buyer,
    uint256 _value,
    uint32 _timestamp,
    uint32 _transactionExpiry,
    uint _fees
    ) payable public {
      bytes32 _hashed = getHashed(_seller, _buyer, _value, _timestamp, _fees);

      require(_tradeID == _hashed);
      require(msg.sender != _buyer);
      require(!escrows[_tradeID]._isActive);
      require(msg.value > 0 && msg.value == _value);
      require(block.timestamp < _transactionExpiry);

      escrows[_tradeID] = Escrow(
        true,
        uint32(block.timestamp) + buyerPaymentWindow,
        0
      );

      emit LogCreated(_tradeID, _buyer, _seller);
  }

  uint256 constant GAS_USED_RELEASE = 50000;
  function release(
    bytes32 _tradeID,
    address _seller,
    address _buyer,
    uint256 _value,
    uint32 _timestamp,
    uint _fees
    ) onlyRelayer external {
      bytes32 _hashed = getHashed(_seller, _buyer, _value, _timestamp, _fees);

      require(_tradeID == _hashed);
      require(escrows[_hashed]._isActive);

      uint256 _totalGasFees = escrows[_hashed]._gasUsedByRelayer.add(GAS_USED_SELLER_CANCEL.mul(uint256(tx.gasprice)));

      transferWithFees(_buyer, _value, _totalGasFees, _fees);
      deleteTrade(_tradeID);

      emit LogFundReleased(_tradeID, _buyer, _seller);
    }

  uint256 constant GAS_USED_BUYER_ACT = 57000;
  function buyerAct(
    bytes32 _tradeID,
    address _seller,
    address _buyer,
    uint256 _value,
    uint32 _timestamp,
    uint _fees
    ) onlyRelayer external {
      bytes32 _hashed = getHashed(_seller, _buyer, _value, _timestamp, _fees);

      require(_tradeID == _hashed);
      require(escrows[_hashed]._isActive);
      require(escrows[_hashed]._enableSellerCancellation != 0);

      escrows[_hashed]._enableSellerCancellation = 0;

      increaseGasUsedByRelayer(_hashed, GAS_USED_BUYER_ACT);

      emit LogBuyerMarkedAsPaidOrOpenDispute(_tradeID);
    }

  uint256 constant GAS_USED_SELLER_CANCEL = 57000;
  function sellerCancel(
    bytes32 _tradeID,
    address _seller,
    address _buyer,
    uint256 _value,
    uint32 _timestamp,
    uint _fees
    ) onlyRelayer external {
      bytes32 _hashed = getHashed(_seller, _buyer, _value, _timestamp, _fees);

      require(_tradeID == _hashed);
      require(escrows[_hashed]._isActive);
      require(escrows[_hashed]._enableSellerCancellation > block.timestamp);

      uint256 _totalGasFees = escrows[_hashed]._gasUsedByRelayer.add(GAS_USED_SELLER_CANCEL.mul(uint256(tx.gasprice)));

      transferWithFees(_seller, _value, _totalGasFees, 0);

      emit LogSellerCancelled(_tradeID, _buyer, _seller);
      deleteTrade(_tradeID);
    }

  uint256 constant GAS_USED_BUYER_CANCEL = 50000;
  function buyerCancel(
    bytes32 _tradeID,
    address _seller,
    address _buyer,
    uint256 _value,
    uint32 _timestamp,
    uint _fees
    ) onlyRelayer external {
      bytes32 _hashed = getHashed(_seller, _buyer, _value, _timestamp, _fees);

      require(_tradeID == _hashed);
      require(escrows[_hashed]._isActive);

      uint256 _totalGasFees = escrows[_hashed]._gasUsedByRelayer.add(GAS_USED_BUYER_CANCEL.mul(uint256(tx.gasprice)));

      transferWithFees(_seller, _value, _totalGasFees, 0);

      emit LogBuyerCancelled(_tradeID, _buyer, _seller);
      deleteTrade(_tradeID);
    }

  uint256 constant GAS_USED_RESOLVED_DISPUTE = 80000;
  function resolveDispute(
    bytes32 _tradeID,
    address _seller,
    address _buyer,
    uint256 _value,
    uint32 _timestamp,
    uint _fees,
    uint8 _v,
    bytes32 _r,
    bytes32 _s,
    uint8 _resolvePercent
  ) onlyRelayer external {

    address _authorizer = getAuthorizer(_tradeID, _v, _r, _s);
    require(_authorizer == _seller || _authorizer == _buyer);

    bytes32 _hashed = getHashed(_seller, _buyer, _value, _timestamp, _fees);

    require(_tradeID == _hashed);
    require(escrows[_hashed]._isActive);
    require(_resolvePercent >= 0 && _resolvePercent <= 100);

    uint256 _totalGasFees = escrows[_hashed]._gasUsedByRelayer.add(GAS_USED_RESOLVED_DISPUTE.mul(uint256(tx.gasprice)));
    ethyneCollectedFees = ethyneCollectedFees.add(_totalGasFees);

    deleteTrade(_tradeID);
    transferDispute(_seller, _buyer, _value, _totalGasFees, _resolvePercent, _fees);

    emit LogResolveDispute(_tradeID, _buyer, _seller);
  }

  // :=== Utilities functions below this line ===:
  function getHashed(
    address _seller,
    address _buyer,
    uint256 _value,
    uint32 _timestamp,
    uint _fees) private pure returns (bytes32) {
    bytes32 _hashed = keccak256(abi.encodePacked(_seller, _buyer, _value, _timestamp, _fees));
    return _hashed;
  }

  function deleteTrade(bytes32 _tradeID) private {
    escrows[_tradeID]._isActive = false;
    delete escrows[_tradeID];
  }

  function increaseGasUsedByRelayer(bytes32 _tradeID, uint256 _totalGas) private {
    escrows[_tradeID]._gasUsedByRelayer = escrows[_tradeID]._gasUsedByRelayer.add(_totalGas);
  }

  function transferWithFees(address _to, uint256 _value, uint256 _totalGasUsedByRelayer, uint _fees) private {
      uint256 _finalFees = (_value.mul(_fees).div(1000)).add(_totalGasUsedByRelayer);
      ethyneCollectedFees = _finalFees.add(ethyneCollectedFees);
      uint256 _finalValue = _value - _finalFees;
      _to.transfer(_finalValue);
  }

  function transferDispute(address _seller, address _buyer, uint256 _value, uint256 _totalGasFees,
    uint8 _resolvePercent, uint _fees) private {
      if(_resolvePercent > 0) {
        uint256 _resolvedValue = ((_value - _totalGasFees) * _resolvePercent / 100);
        uint256 _finalFees = (_resolvedValue.mul(_fees).div(1000)).add(_totalGasFees);
        _buyer.transfer(_resolvedValue.sub(_finalFees));
      }
      if(_resolvePercent < 100) {
        _seller.transfer((_value - _totalGasFees) * (100 - _resolvePercent) / 100);
      }
  }

  // :=== the functions below this line will be related to the company ===:

  function setOwner(address _newOwner) onlyOwner external {
    //Change the owner of the contract
    //Can be called by owner only
    owner = _newOwner;
  }

  // withdraw the revenue from trading to specific account
  function getRevenue() onlyOwner view external returns(uint256) {
    return ethyneCollectedFees;
  }

  function withdrawRevenue(address _to, uint256 _amount) onlyOwner external {
    require(_amount < ethyneCollectedFees);
    ethyneCollectedFees = ethyneCollectedFees.sub(_amount);
    _to.transfer(_amount);
  }

  function setBuyerPaymentWindow(uint32 _newBuyerPaymentWindow) onlyOwner external {
    buyerPaymentWindow = _newBuyerPaymentWindow;
  }

  function setRelayer(address _newRelayer) onlyOwner external {
    relayer = _newRelayer;
  }

  function getAuthorizer(
    bytes32 _tradeID,
    uint8 _v,
    bytes32 _r,
    bytes32 _s
  ) private pure returns (address) {
    bytes memory _prefix = "\x19Ethereum Signed Message:\n32";
    return ecrecover(keccak256(abi.encodePacked(_prefix, _tradeID)), _v, _r, _s);
  }

}
