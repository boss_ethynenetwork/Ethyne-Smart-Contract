module.exports = {
  EthyneEscrow: require('./build/contracts/EthyneEscrow.json'),
  SafeMath: require('./build/contracts/SafeMath.json'),
  Migrations: require('./build/contracts/Migrations.json')
}